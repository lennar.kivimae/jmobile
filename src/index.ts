import $ from 'jquery';
import './index.scss';

interface ILoginResponse {
    status: string;
}

interface IOrderClassic {
    username: string;
    size: string;
    selection: string;
    milk: string;
}

interface IOrderCustom {
    username: string;
    size: string;
    selection: string;
    milk: string;
    additives: string;
    syrup: string;
    foam: string;
    balance: string;
}

class Ajax {
    static path = `${document.URL}assets/api/ajax.php`;
}

class Pay {
    private element: JQuery = $('.pay');
    private ajax: JQuery.jqXHR;

    constructor(ajax: JQuery.jqXHR) {
        this.ajax = ajax;
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.element.addClass('is-hidden');
                $('.order-status').removeClass('is-hidden');
            } else {
                alert('Something went wrong :/, We are working on it');
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to register, please try again later :(');
        });
    }
}

class Final {
    private element: JQuery = $('.final');
    private summaryContainer: JQuery;
    private orderType: string;
    private orderClassic: IOrderClassic;
    private orderCustom: IOrderCustom;
    private ajax: JQuery.jqXHR;

    constructor() {
        this.orderType = $('[data-type]').data('type');
        this.summaryContainer = this.element.find(`.final__details`);
        this.init();
    }

    removeClasses(element: JQuery): JQuery {
        return element.removeClass().addClass('final__item');
    }

    init() {
        this.element.removeClass('is-hidden');
        this.element.find(`.final__button`).on('click.final', this.sendOrder.bind(this));
        this.buildSummary();
    }

    sendOrder(): void {
        this.element.addClass('is-hidden');

        if (this.ajax) {
            this.ajax.abort();
        }

        const payload: IOrderClassic | IOrderCustom = this.orderType === 'own' ? this.orderCustom : this.orderClassic;

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'order',
                order: payload
            }
        });

        new Pay(this.ajax);
    }

    buildSummary(): void {
        if (this.orderType === 'own') {
            this.buildCustomSummary();

            return;
        }

        this.buildClassicSummary();
    }

    buildCustomSummary(): void {
        const size: JQuery = this.removeClasses($('.size').find(`.is-selected`).clone());
        const selection: JQuery = this.removeClasses($('.choose-selection').find(`.is-selected`).clone()).addClass('background--dark');
        const milk: JQuery = this.removeClasses($('.milk-selection').find(`.is-selected`).clone());
        const syrup: JQuery = this.removeClasses($('.syrup').find(`.is-selected`).clone()).addClass('background--dark');
        const additives: JQuery = this.removeClasses($('.additives').find(`.is-selected`).clone());
        const balance: string = $(`[data-milkbalance]`).data('milkbalance');
        const foam: string = $(`[data-milkfoam]`).data('milkfoam');

        this.summaryContainer.append(size);
        this.summaryContainer.append(selection);
        this.summaryContainer.append(milk);
        this.summaryContainer.append(syrup);
        this.summaryContainer.append(additives);
        this.summaryContainer.append(this.createElement('Balance', balance + ' %', 'background--dark'));
        this.summaryContainer.append(this.createElement('Foam', foam + ' %'));

        // Build data for AJAX
        this.orderCustom = {
            username: Login.username,
            size: size.find(`p`).text(),
            milk: milk.find('p').text(),
            selection: milk.find('p').text(),
            additives: additives.text(),
            syrup: syrup.text(),
            balance: balance,
            foam: foam,
        }
    }

    createElement(text: string, value: string, modifier?: string): JQuery {
        const element = `
            <p class="final__item ${modifier}">
                <span class="final-item__text">${text}</span>
                <span class="final-item__value">${value}</span>
            </p>
        `;

        return $(element);
    }

    buildClassicSummary(): void {
        const size: JQuery = this.removeClasses($('.size').find(`.is-selected`).clone());
        const selection: JQuery = this.removeClasses($('.choose-selection').find(`.is-selected`).clone()).addClass('background--dark');
        const milk: JQuery = this.removeClasses($('.milk-selection').find(`.is-selected`).clone());

        this.summaryContainer.append(size);
        this.summaryContainer.append(selection);
        this.summaryContainer.append(milk);

        // Build data for ajax
        this.orderClassic = {
            username: Login.username,
            size: size.find(`p`).text(),
            milk: milk.find('p').text(),
            selection: milk.find('p').text()
        }
    }
}

class Additives {
    private element: JQuery = $('.additives');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.additives__option`).on('click.additives', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);

        clickedElement.addClass('is-selected');
        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();
        new Final();
    }

    destroy(): void {
        this.element.find(`.additives__option`).off('click.additives');
    }
}

class Syrup {
    private element: JQuery = $('.syrup');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.syrup__option`).on('click.syrup', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);

        clickedElement.addClass('is-selected');
        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();
        new Additives();
    }

    destroy(): void {
        this.element.find(`.syrup__option`).off('click.syrup');
    }
}

class MilkFoam {
    private element: JQuery = $('.milk-foam');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-foam__slider`).on('input.milk-foam', this.updatePercentage.bind(this));
        this.element.find(`.milk-foam__next`).on('click.milk-foam', this.triggerNavigation.bind(this));
    }

    updatePercentage(event: JQuery.TriggeredEvent): void {
        const balanceValue: number = Number($(event.currentTarget).val());

        this.element.find(`.milk-foam__percentage`).text(`${balanceValue} %`);
        this.element.get(0).dataset.milkfoam = String(balanceValue);
    }

    triggerNavigation(event: JQuery.ClickEvent): void {
        event.preventDefault();

        this.element.addClass('is-hidden');
        this.destroy();
        new Syrup();
    }

    destroy(): void {
        this.element.find(`.milk-foam__next`).off('click.milk-foam');
        this.element.find(`.milk-foam__slider`).off('change.milk-foam');
    }
}

class MilkBalance {
    private element: JQuery = $('.milk-balance');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-balance__slider`).on('input.milk-balance', this.updatePercentage.bind(this));
        this.element.find(`.milk-balance__next`).on('click.milk-balance', this.triggerNavigation.bind(this));
    }

    updatePercentage(event: JQuery.TriggeredEvent): void {
        const balanceValue: number = Number($(event.currentTarget).val());

        this.element.find(`.milk-balance__percentage`).text(`${balanceValue} %`);
        this.element.get(0).dataset.milkbalance = String(balanceValue);
    }

    triggerNavigation(event: JQuery.ClickEvent): void {
        event.preventDefault();

        this.element.addClass('is-hidden');
        this.destroy();
        new MilkFoam();
    }

    destroy(): void {
        this.element.find(`.milk-balance__next`).off('click.milk-balance');
        this.element.find(`.milk-balance__slider`).off('change.milk-balance');
    }
}

class MilkSelection {
    private element: JQuery = $('.milk-selection');

    private orderType: string;

    constructor() {
        this.orderType = $('[data-type]').data('type');
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-selection__option`).on('click.milk-selection', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        clickedElement.addClass('is-selected');

        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();

        if (this.orderType === 'own') {
            new MilkBalance();

            return;
        }

        //if classic order type -> to final
        new Final();
    }

    destroy(): void {
        this.element.find(`.milk-selection__option`).off('click.milk-selection');
    }
}

class ChooseSelection {
    private element: JQuery = $('.choose-selection');

    private orderType: string;

    constructor() {
        this.orderType = $('[data-type]').data('type');

        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.showSelection();
        this.addClickEvents();
    }

    addClickEvents(): void {
        this.element.find(`.choose-selection__option`).on('click.choose-selection', this.selectHandler.bind(this));
    }

    selectHandler(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);

        clickedElement.addClass('is-selected');

        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();
        new MilkSelection();
    }

    showSelection(): void {
        if (this.orderType === 'own') {
            this.element.find(`.choose-selection__options`).addClass('is-hidden');
            this.element.find('.choose-selection--own').removeClass('is-hidden');
        }
    }

    destroy(): void {
        this.element.find(`.choose-selection__option`).off('click.choose-selection');
    }
}

class Size {
    private element: JQuery = $('.size');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.size-options__option`).on('click.size', this.chooseSize.bind(this));
    }

    chooseSize(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        const chosenSize: string = clickedElement.find(`.size-options__text`).text();

        clickedElement.addClass('is-selected');
        this.element.get(0).dataset.size = chosenSize;

        this.element.addClass('is-hidden');
        this.destroy();
        new ChooseSelection();
    }

    destroy(): void {
        this.element.find(`.size-options__option`).off('click.size');
    }
}

class Choose {
    private element: JQuery = $('.choose');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find('.choose__classic').on('click.choose', this.chooseType.bind(this));
        this.element.find('.choose__own').on('click.choose', this.chooseType.bind(this));
    }

    chooseType(event: JQuery.ClickEvent): void {
        const currentTarget = $(event.currentTarget);

        if (currentTarget.hasClass('choose__classic')) {
            this.element.get(0).dataset.type = 'classic';
        } else {
            this.element.get(0).dataset.type = 'own';
        }

        new Size();
        this.element.addClass('is-hidden');
        this.destroy();
    }

    destroy(): void {
        this.element.off('click.choose');
    }
}

class Login {
    private element: JQuery = $('.login');

    private ajax: JQuery.jqXHR;

    static username: string;

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.textfield`).on('click.login', this.labelHandler.bind(this));
        this.element.find(`.login__login`).on('click.login', this.login.bind(this));
        this.element.find(`.login__register`).on('click.login', this.register.bind(this));
    }

    destroy(): void {
        this.element.off('click.login');
    }

    labelHandler(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        const clickedElementClass: string = clickedElement.attr('class').replace(' textfield', '');

        this.element.find(`[data-label="${clickedElementClass}"]`).addClass('is-hidden');
    }

    login(event: JQuery.ClickEvent): void {
        event.preventDefault();

        const username: string = String(this.element.find(`.login__username`).val());
        const password: string = String(this.element.find(`.login__password`).val());

        if (this.ajax) {
            this.ajax.abort();
        }

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'login',
                username: username,
                password: password
            }
        });

        //login logic
        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.destroy();
                this.element.addClass('is-hidden');
                Login.username = username;
                new Choose();
            } else {
                alert('Wrong username or password, please try again');
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to login, please try again later :(');
        });
    }

    register(event: JQuery.ClickEvent): void {
        event.preventDefault();

        const username: string = String(this.element.find(`.login__username`).val());
        const password: string = String(this.element.find(`.login__password`).val());

        if (this.ajax) {
            this.ajax.abort();
        }

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'add-user',
                username: username,
                password: password
            }
        });

        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.destroy();
                this.element.addClass('is-hidden');
                Login.username = username;
                new Choose();
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to register, please try again later :(');
        });
    }
}

class Page {
    private element: JQuery = $('.page');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.find(`.loading`).addClass('is-hidden');
        new Login();
    }
}

$(() => {
    new Page();
});
